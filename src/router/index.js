import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
  {
    path: '/list',
    name: 'List',
    component: () => import(/* webpackChunkName: "about" */ '../views/ListView')
  },
  {
    path: '/list/:id',
    name: 'ListItem',
    component: () => import(/* webpackChunkName: "about" */ '../views/ListItemView'),
    props: true
  },
  {
    path: '*',
    redirect: '/list'
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
