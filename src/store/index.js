import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const posts = [
    {
        "id": "1",
        "title": "3 Habits of Incredibly Healthy People",
        "description": "No, you don’t have to go to the gym.",
        "imageUrl": "https://miro.medium.com/max/1400/0*N8m6WUQQDytZVzsu",
        "date": "2021-01-01"
    },
    {
        "id": "2",
        "title": "My Husband is Useless and Does Nothing",
        "description": null,
        "imageUrl": "https://miro.medium.com/max/1400/0*dKb2wnAtSLG8HZWD",
        "date": "2021-01-02"
    },
    {
        "id": "3",
        "title": "I’ll send my M1 Apple back. It’s not ready yet.",
        "description": "As an engineer / scientist, the system is too problematic for work to be feasible.",
        "imageUrl": "https://miro.medium.com/max/1400/1*z1nuXiZlCcUn9lb2HhQwKA.png",
        "date": "2021-01-02"
    },
    {
        "id": "4",
        "title": "4 Harmful Myths That Hurt Cats",
        "description": "Debunking common misunderstandings with actual scientific explanations for feline behavior",
        "imageUrl": "https://miro.medium.com/max/2000/1*hL3bY7e3HccsEthuI1lvcg.jpeg",
        "date": "2021-01-02"
    },
    {
        "id": "5",
        "title": "8 Frontend Coding Ideas That Will Inspire You To Code",
        "description": "Get inspired to code by these exciting coding ideas",
        "imageUrl": "https://miro.medium.com/max/1400/0*GGc7BlFTO6PwC7eg",
        "date": "2021-01-03"
    },
    {
        "id": "6",
        "title": "8 React Libraries That I’d Like To Introduce To You",
        "description": null,
        "imageUrl": "https://miro.medium.com/max/1400/0*1zDWU9G0d6XeVgYO",
        "date": "2021-01-05"
    },
    {
        "id": "7",
        "title": "I’ll Never Be a Chief Technology Officer Again… Unless It’s for My Own Company",
        "description": "Here’s why.",
        "imageUrl": "https://miro.medium.com/max/1400/1*JITGktr2nNrQCOzAZ-racw.jpeg",
        "date": "2021-01-05"
    }
];

const fakeRequest = (data, delay = 0) => new Promise(resolve => setTimeout(() => resolve({data}), delay));

export default new Vuex.Store({
    state: {
        posts: [],
        post: {}
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        },

        setPost (state, post) {
            state.post = post
        }
    },
    getters: {
        getPost: (state) => id => state.posts.find(post => post.id === id)
    },
    actions: {
        async fetchPosts({commit}) {
            try {
                let res = await fakeRequest(posts, 1500);

                // let res = await axios.get('https://json-parser.com/f2c743cd/1.json', {
                //     crossdomain: true,
                //     headers: {
                //         'Access-Control-Allow-Origin': '*'
                //     }
                // });

                commit('setPosts', res.data)
            } catch
                (error) {
                console.log('setPosts commit', error);
            }
        },

        async fetchPost({commit}, id) {
            try {
                const post = posts.find(post => post.id === id) || {};

                let res = await fakeRequest(post, 1500);

                commit('setPost', res.data)
            } catch (error) {
                console.log('setPost commit', error);
            }
        }
    }
})
